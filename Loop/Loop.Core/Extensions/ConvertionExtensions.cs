﻿using Loop.Core.Infrastructure;
using Loop.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Loop.Core.Extensions
{
    public static class ConvertionExtensions
    {
        #region string

        public static int ToInt(this string value, int defaultValue = 0)
        {
            int result;
            if (Int32.TryParse(value, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static char ToChar(this string value, bool unescape = false, char defaultValue = '\0')
        {
            char result;
            if (value.HasValue() && char.TryParse(unescape ? Regex.Unescape(value) : value, out result))
            {
                return result;
            }
            return defaultValue;
        }

        public static float ToFloat(this string value, float defaultValue = 0)
        {
            float result;
            if (float.TryParse(value, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static bool ToBool(this string value, bool defaultValue = false)
        {
            bool result;
            if (bool.TryParse(value, out result))
            {
                return result;
            }

            return defaultValue;
        }

        public static DateTime? ToDateTime(this string value, DateTime? defaultValue)
        {
            return value.ToDateTime(null, defaultValue);
        }

        public static DateTime? ToDateTime(this string value, string[] formats, DateTime? defaultValue)
        {
            return value.ToDateTime(formats, DateTimeFormatInfo.InvariantInfo, DateTimeStyles.AllowWhiteSpaces, defaultValue);
        }

        public static DateTime? ToDateTime(this string value, string[] formats, IFormatProvider provider, DateTimeStyles styles, DateTime? defaultValue)
        {
            DateTime result;

            if (formats.IsNullOrEmpty())
            {
                if (DateTime.TryParse(value, provider, styles, out result))
                {
                    return result;
                }
            }

            if (DateTime.TryParseExact(value, formats, provider, styles, out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
		/// Parse ISO-8601 UTC timestamp including milliseconds.
		/// </summary>
		/// <remarks>
		/// Dublicate can be found in HmacAuthentication class.
		/// </remarks>
		public static DateTime? ToDateTimeIso8601(this string value)
        {
            if (value.HasValue())
            {
                DateTime dt;
                if (DateTime.TryParseExact(value, "o", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt))
                    return dt;

                if (DateTime.TryParseExact(value, "yyyy-MM-ddTHH:mm:ss.fffZ", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dt))
                    return dt;
            }
            return null;
        }

        [DebuggerStepThrough]
        public static Version ToVersion(this string value, Version defaultVersion = null)
        {
            try
            {
                return new Version(value);
            }
            catch
            {
                return defaultVersion ?? new Version("1.0");
            }
        }

        #endregion

        #region int

        public static char ToHex(this int value)
        {
            if (value <= 9)
            {
                return (char)(value + 48);
            }
            return (char)((value - 10) + 97);
        }

        #endregion

        #region Object

      

 
 
   

        #endregion

        #region Stream

        public static byte[] ToByteArray(this Stream stream)
        {
            Check.NotNull(stream, nameof(stream));

            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            else
            {
                using (var streamReader = new MemoryStream())
                {
                    stream.CopyTo(streamReader);
                    return streamReader.ToArray();
                }
            }
        }

        public static async Task<byte[]> ToByteArrayAsync(this Stream stream)
        {
            Check.NotNull(stream, nameof(stream));

            if (stream is MemoryStream)
            {
                return ((MemoryStream)stream).ToArray();
            }
            else
            {
                using (var streamReader = new MemoryStream())
                {
                    await stream.CopyToAsync(streamReader);
                    return streamReader.ToArray();
                }
            }
        }

        public static string AsString(this Stream stream)
        {
            return stream.AsString(Encoding.UTF8);
        }

        public static Task<string> AsStringAsync(this Stream stream)
        {
            return stream.AsStringAsync(Encoding.UTF8);
        }

        public static string AsString(this Stream stream, Encoding encoding)
        {
            Check.NotNull(encoding, nameof(encoding));

            // convert stream to string
            string result;

            if (stream.CanSeek)
            {
                stream.Position = 0;
            }

            using (StreamReader sr = new StreamReader(stream, encoding))
            {
                result = sr.ReadToEnd();
            }

            return result;
        }

        public static Task<string> AsStringAsync(this Stream stream, Encoding encoding)
        {
            Check.NotNull(encoding, nameof(encoding));

            // convert stream to string
            Task<string> result;

            if (stream.CanSeek)
            {
                stream.Position = 0;
            }

            using (StreamReader sr = new StreamReader(stream, encoding))
            {
                result = sr.ReadToEndAsync();
            }

            return result;
        }

        #endregion
    }
}
