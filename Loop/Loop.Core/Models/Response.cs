﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Models
{
    public class Response
    {
        /// <summary>
        /// Indica si la operación se realizo correctamente
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Mensaje del resultado de la operación
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Objecto resultante de la operación
        /// </summary>
        public object Result { get; set; }
    }
}
