﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Data
{
    public interface IDataProvider
    {
        /// <summary>
        /// A value indicating whether this data provider supports stored procedures
        /// </summary>
        bool StoredProceduresSupported { get; }

        /// <summary>
		/// Gets the db provider invariant name (e.g. <c>System.Data.SqlClient</c>)
		/// </summary>
		string ProviderInvariantName { get; }

        string ProviderAssemblyQualifiedName { get; }
        string CadConexion { get; }
        int TiempoEspera { get; set; }
        CommandType TipoConsulta { get; set; }

        /// <summary>
        /// Gets a support database parameter object (used by stored procedures)
        /// </summary>
        /// <returns>Parameter</returns>
        DbParameter GetParameter();
        DbParameter CreateParameter(string parameterName, object value);
        int CargarQuery(string sql);
        int CargarQuery(string sqlProc, DbParameter[] parametros);
        DbCommand ComandoProc(string procName, DbParameter[] parameters);
        DbCommand ComandoSQL(string commandText);
        DbCommand ComandoTransact();
        DbCommand GetCommand();
        DataTable getDataTable(string sql);
        DataTable getDataTable(string sqlProc, DbParameter[] parametros);
        string GetJSON(string sql);
        string GetJSON(string sqlProc, DbParameter[] parametros);
        string GetJSON_CampoValor(string sql);
        string GetJSON_CampoValor(string sql, string JsonName);
        string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros);
        string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros, string JsonName);
        DbDataReader getReader(string sql);
        DbDataReader getReader(string sqlProc, DbParameter[] parameters);
        object getScalar(string sql);
        object getScalar(string sqlProc, DbParameter[] parametros);
        bool insertQuerys(List<string> querys);
        int insertReturnID(string sql);
        int insertReturnID(string sql, DbParameter[] parametros);
    }
}
