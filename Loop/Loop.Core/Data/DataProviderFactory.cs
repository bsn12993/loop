﻿using Loop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Data
{
    public abstract class DataProviderFactory
    {
        protected DataProviderFactory(DataSettings settings)
        {
            Check.NotNull(settings, nameof(settings));
            this.Settings = settings;
        }

        protected DataSettings Settings
        {
            get;
            private set;
        }

        public abstract IDataProvider loadDataProvider();
    }
}
