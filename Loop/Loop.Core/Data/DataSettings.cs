﻿using Loop.Core.Extensions;
using Loop.Core.Infrastructure;
using Loop.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Loop.Core.Data
{
    public class DataSettings
    {
        private static DataSettings s_current = null;

        #region AppSetting
        public static string Environment
        {
            get { return ConfigurationManager.AppSettings["Environment"].ToString(); }
        }
        public static string DatabaseEngine
        {
            get { return ConfigurationManager.AppSettings["DatabaseEngine"].ToString(); }
        }
        #endregion

        #region ConnectionString
        public static string CnnDev
        {
            get { return ConfigurationManager.ConnectionStrings["DEV"].ConnectionString; }
        }
        public static string CnnQa
        {
            get { return ConfigurationManager.ConnectionStrings["QA"].ConnectionString; }
        }
        public static string CnnProd
        {
            get { return ConfigurationManager.ConnectionStrings["PROD"].ConnectionString; }
        }
        #endregion

        public Version AppVersion { get; set; }
         

        public static DataSettings Current
        {
            get
            {
                if (s_current == null) s_current = new DataSettings();
                return s_current;
            }
        }


        public bool IsSqlServer
        {
            get
            {
                return Environment.HasValue() && Environment.ToLower().StartsWith("sqlserver", StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public bool IsMySQLServer
        {
            get
            {
                return Environment.HasValue() && Environment.ToLower().StartsWith("mysqlserver", StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public string DataConnectionString
        {
            get
            {
                string cnn = string.Empty;
                if (Environment.ToLower().Equals("DEV".ToLower())) cnn = CnnDev;
                else if (Environment.ToLower().Equals("QA".ToLower())) cnn = CnnQa;
                else cnn = CnnProd;
                return cnn;
            }
        }

        public string DataConnectionType { get; set; }

        public bool IsValid()
        {
            return Environment.HasValue() && this.DataConnectionString.HasValue();
        }
    
    }
}