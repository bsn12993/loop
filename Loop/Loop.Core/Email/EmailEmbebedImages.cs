﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Email
{
    public class EmailEmbebedImages
    {
        public string PathDir { get; private set; }
        public string cId { get; private set; }

        public EmailEmbebedImages(string _path, string _cId)
        {
            //this.PathDir = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @_path);
            this.PathDir = Path.Combine(Environment.CurrentDirectory, @_path);
            this.cId = _cId;
        }
    }
}
