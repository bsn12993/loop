﻿using Loop.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Email
{
    public class EmailAccount  
    {
        public EmailAccount()
        {
        }

        public string Name { get; set; }
        public string Email { get; set; }

        public string Host { get; set; }
        public int Port { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
        public bool UseDefaultCredentials { get; set; }

        public string FriendlyName
        {
            get
            {
                if (Name.IsEmpty())
                {
                    return Email;
                }

                return "{0} ({1})".FormatInvariant(Name, Email);
            }
        }
    }
}
