﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Email
{
    public interface IEmailSender
    {
        void SendMail(SmtpContext smtpContext, EmailMessage message);
        Task SendEmailAsync(SmtpContext smtpContext, EmailMessage message);
        void SendMail(EmailMessage message);
        Task SendEmailAsync(EmailMessage message);
    }
}
