﻿using Loop.Core.Extensions;
using Loop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Core.Email
{
    public class EmailSender : IEmailSender
    {
        /// <summary>
        /// Send email with specific account of smtp 
        /// </summary>
        public void SendMail(SmtpContext context, EmailMessage message)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(message, nameof(message));

            using (var msg = this.BuildMailMessage(message))
            {
                using (var client = context.ToSmtpClient())
                {
                    client.Send(msg);
                }
            }
        }

        /// <summary>
        /// Send email with web.config account of smtp 
        /// </summary>
        public void SendMail(EmailMessage message)
        {
            Check.NotNull(message, nameof(message));

            var msg = this.BuildMailMessage(message, Encoding.UTF8);
            using (SmtpClient client = new SmtpClient())
            {
                client.Send(msg);
            }
        }

        public Task SendEmailAsync(SmtpContext context, EmailMessage message)
        {
            Check.NotNull(context, nameof(context));
            Check.NotNull(message, nameof(message));

            var client = context.ToSmtpClient();
            var msg = this.BuildMailMessage(message);

            return client.SendMailAsync(msg).ContinueWith(t =>
            {
                client.Dispose();
                msg.Dispose();
            });
        }

        public Task SendEmailAsync(EmailMessage message)
        {
            Check.NotNull(message, nameof(message));

            var msg = this.BuildMailMessage(message);

            using (SmtpClient client = new SmtpClient())
            {
                return client.SendMailAsync(msg).ContinueWith(t =>
                {
                    client.Dispose();
                    msg.Dispose();
                });
            }
        }

        protected virtual MailMessage BuildMailMessage(EmailMessage original, Encoding encodingType = null)
        {
            MailMessage msg = new MailMessage();

            if (String.IsNullOrEmpty(original.Subject))
            {
                throw new MailSenderException("Required subject is missing!");
            }

            msg.Subject = original.Subject;
            msg.IsBodyHtml = original.BodyFormat == MailBodyFormat.Html;

            if (msg.IsBodyHtml = original.BodyFormat == MailBodyFormat.Html)
            {
                var viewTextHtml = AlternateView.CreateAlternateViewFromString(original.Body, new ContentType("text/html"));

                //si existen imagenes embebidas agregarlas
                if (original.EmbebedImages.Any())
                {
                    original.EmbebedImages.Each(img =>
                    {
                        var EmbebedImg = new LinkedResource(img.PathDir, "image/png");
                        EmbebedImg.ContentId = img.cId;
                        viewTextHtml.LinkedResources.Add(EmbebedImg);
                    });
                }

                msg.AlternateViews.Add(viewTextHtml);

                //Si tiene una vista alterna con texto plano agregarla
                if (original.AltText.HasValue())
                {
                    msg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(original.AltText, new ContentType("text/plain")));
                }
            }
            else
            {
                msg.Body = original.Body;
            }

            msg.DeliveryNotificationOptions = DeliveryNotificationOptions.None;

            msg.From = original.From.ToMailAddress();

            msg.To.AddRange(original.To.Where(x => x.Address.HasValue()).Select(x => x.ToMailAddress()));
            msg.CC.AddRange(original.Cc.Where(x => x.Address.HasValue()).Select(x => x.ToMailAddress()));
            msg.Bcc.AddRange(original.Bcc.Where(x => x.Address.HasValue()).Select(x => x.ToMailAddress()));
            msg.ReplyToList.AddRange(original.ReplyTo.Where(x => x.Address.HasValue()).Select(x => x.ToMailAddress()));

            msg.Attachments.AddRange(original.Attachments);

            if (original.Headers != null)
                msg.Headers.AddRange(original.Headers);


            msg.Priority = original.Priority;

            if (encodingType != null)
            {
                msg.BodyEncoding = encodingType;
            }
            return msg;
        }
    }
}
