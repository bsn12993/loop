﻿using Loop.Core.Extensions;
using Loop.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace Loop.Core.Utilities
{
    public static partial class CommonHelper
    {
        public static string MapPath(string path, bool findAppRoot = true)
        {
            Check.NotNull(path, nameof(path));

            if (HostingEnvironment.IsHosted)
            {
                // Alojado
                return HostingEnvironment.MapPath(path);
            }
            else
            {
                // No alojado. Por ejemplo, ejecutar pruebas unitarias o herramientas EF
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                path = path.Replace("~/", "").TrimStart('/').Replace('/', '\\');

                var testPath = Path.Combine(baseDirectory, path);

                if (findAppRoot /* && !Directory.Exists(testPath)*/)
                {
                    // Lo más probable es que estemos en pruebas unitarias o en modo de diseño (andamio de migración de EF) 
                    // Encuentre primero el directorio raíz de la solución.
                    var dir = FindSolutionRoot(baseDirectory);

                    // concatena la raiz web
                    if (dir != null)
                    {
                        baseDirectory = Path.Combine(dir.FullName, "CCO\\CCO.Web");
                        testPath = Path.Combine(baseDirectory, path);
                    }
                }

                return testPath;
            }
        }

        private static DirectoryInfo FindSolutionRoot(string currentDir)
        {
            var dir = Directory.GetParent(currentDir);
            while (true)
            {
                if (dir == null || IsSolutionRoot(dir))
                    break;

                dir = dir.Parent;
            }

            return dir;
        }

        private static bool IsSolutionRoot(DirectoryInfo dir)
        {
            return File.Exists(Path.Combine(dir.FullName, "CCO.sln"));
        }
    }
}
