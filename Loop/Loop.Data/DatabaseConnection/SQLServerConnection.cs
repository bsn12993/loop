﻿using Loop.Core.Data;
using Loop.Data.Interface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.DatabaseConnection
{
    public class SQLServerConnection : IConnection
    {
        DatabaseConnectionParams DatabaseConnectionParams = null;

        public SQLServerConnection()
        {
            DbConnectionDefault();
        }

        public void DbConnectionDefault()
        {
            DatabaseConnectionParams = new DatabaseConnectionParams();
            DatabaseConnectionParams.TiempoEspera = 300;
            DatabaseConnectionParams.TipoConsulta = CommandType.Text;
            DatabaseConnectionParams.CadConexion = DataSettings.Current.DataConnectionString;
        }

        public void DbConnectionConnectionString(string keyCadConexion)
        {
            DatabaseConnectionParams = new DatabaseConnectionParams();
            DatabaseConnectionParams.TiempoEspera = 300;
            DatabaseConnectionParams.TipoConsulta = CommandType.Text;
            DatabaseConnectionParams.CadConexion = Convert.ToString(ConfigurationManager.ConnectionStrings[keyCadConexion]);
        }

        public void DbConnectionParams(string host, string bd, bool sspi, string user = null, string password = null)
        {
            DatabaseConnectionParams = new DatabaseConnectionParams();
            DatabaseConnectionParams.TiempoEspera = 300;
            DatabaseConnectionParams.TipoConsulta = CommandType.Text;

            if (sspi)
            {
                DatabaseConnectionParams.CadConexion = string.Format("Persist Security Info=False;Data Source={0};Initial Catalog={1};Integrated Security=SSPI", host, bd);
            }
            else
            {
                DatabaseConnectionParams.CadConexion = string.Format("Persist Security Info=False;Data Source={0};Initial Catalog={1};user id={2};password={3};", host, bd, user, password);
            }
        }

        public DbConnection ObtenerConexion()
        {
            DbConnection Conexion = new SqlConnection(DatabaseConnectionParams.CadConexion);
            return Conexion;
        }

        public bool ProbarConexion()
        {
            bool ConexionValida = false;
            DbConnection Conexion = ObtenerConexion();
            try
            {
                Conexion.Open();
                Conexion.Close();
                ConexionValida = true;
            }
            catch
            {
                ConexionValida = false;
            }

            return ConexionValida;
        }

        public DbCommand ComandoProc(string sqlProc, DbParameter[] Parametros)
        {
            DbConnection Conexion = ObtenerConexion();
            DbCommand comando = new SqlCommand(sqlProc, (SqlConnection)Conexion) { CommandType = DatabaseConnectionParams.TipoConsulta, CommandTimeout = DatabaseConnectionParams.TiempoEspera };
            comando.Parameters.AddRange(Parametros);
            return comando;
        }

        public DbCommand ComandoSQL(string sql)
        {
            DbConnection Conexion = ObtenerConexion();
            DbCommand comando = new SqlCommand(sql, (SqlConnection)Conexion) { CommandType = CommandType.Text, CommandTimeout = DatabaseConnectionParams.TiempoEspera };
            return comando;
        }

        public DbCommand ComandoTransact()
        {
            DbConnection Conexion = ObtenerConexion();
            DbTransaction transaccion = null;
            Conexion.Open();
            transaccion = Conexion.BeginTransaction(IsolationLevel.RepeatableRead);
            DbCommand comando = new SqlCommand() { CommandType = CommandType.Text, CommandTimeout = DatabaseConnectionParams.TiempoEspera, Transaction = (SqlTransaction)transaccion, Connection = (SqlConnection)Conexion };
            return comando;
        }

        public DbDataReader getReader(string sqlProc, DbParameter[] parametros)
        {
            DbCommand comando = ComandoProc(sqlProc, parametros);
            DbDataReader dr = null;
            comando.Connection.Open();
            dr = comando.ExecuteReader(CommandBehavior.CloseConnection);
            return dr;
        }

        public DbDataReader getReader(string sql)
        {
            DbCommand comando = ComandoSQL(sql);
            DbDataReader dr = null;
            comando.Connection.Open();
            dr = comando.ExecuteReader(CommandBehavior.CloseConnection);
            return dr;
        }

        public DataTable getDataTable(string sqlProc, DbParameter[] parametros)
        {
            DbCommand comando = ComandoProc(sqlProc, parametros);
            SqlDataAdapter da = new SqlDataAdapter((SqlCommand)comando);
            DataTable dt = new DataTable();
            try
            {
                comando.Connection.Open();
                da.Fill(dt);
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }
            return dt;
        }

        public DataTable getDataTable(string sql)
        {
            DbCommand comando = ComandoSQL(sql);
            DbDataAdapter da = new SqlDataAdapter((SqlCommand)comando);
            DataTable dt = new DataTable();
            try
            {
                comando.Connection.Open();
                da.Fill(dt);
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }
            return dt;
        }

        public int CargarQuery(string sqlProc, DbParameter[] parametros)
        {
            DbCommand comando = ComandoProc(sqlProc, parametros);
            int ColumnasAfectadas = 0;
            try
            {
                comando.Connection.Open();
                ColumnasAfectadas = comando.ExecuteNonQuery();
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }

            return ColumnasAfectadas;
        }

        public int CargarQuery(string sql)
        {
            DbCommand comando = ComandoSQL(sql);
            int ColumnasAfectadas = 0;
            try
            {
                comando.Connection.Open();
                ColumnasAfectadas = comando.ExecuteNonQuery();
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }

            return ColumnasAfectadas;
        }

        public int insertReturnID(string sql)
        {
            DbCommand comando = ComandoSQL(sql);
            int idGenerado = 0;
            try
            {
                comando.Connection.Open();
                comando.ExecuteNonQuery();
                comando.CommandText = "select SCOPE_IDENTITY()";
                idGenerado = Convert.ToInt32(comando.ExecuteScalar());
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }

            return idGenerado;
        }

        public int insertReturnID(string sql, DbParameter[] parametros)
        {
            DbCommand comando = null;
            int idGenerado = 0;
            try
            {
                sql += "; select SCOPE_IDENTITY()";
                comando = ComandoProc(sql, parametros);
                comando.Connection.Open();
                idGenerado = Convert.ToInt32(comando.ExecuteScalar());
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }

            return idGenerado;
        }

        public bool insertQuerys(List<string> querys)
        {
            DbCommand comando = new SqlCommand();
            DbConnection Conexion = new SqlConnection(DatabaseConnectionParams.CadConexion);
            DbTransaction transaccion = null;
            bool result = false;
            try
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }

                transaccion = Conexion.BeginTransaction(IsolationLevel.RepeatableRead);
                comando.Transaction = transaccion;
                comando.CommandTimeout = DatabaseConnectionParams.TiempoEspera;
                comando.CommandType = CommandType.Text;
                comando.Connection = Conexion;

                foreach (string query in querys)
                {
                    comando.CommandText = query;
                    comando.ExecuteNonQuery();
                    result = true;
                }

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                transaccion.Rollback();
                throw ex;
            }
            finally
            {
                if (comando.Connection != null) Conexion.Close();
            }

            return result;
        }

        public object getScalar(string sqlProc, DbParameter[] parametros)
        {
            object resultado = null;
            DbCommand comando = ComandoProc(sqlProc, parametros);
            try
            {
                comando.Connection.Open();
                resultado = comando.ExecuteScalar();
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }
            return resultado;
        }

        public object getScalar(string sql)
        {
            object resultado = null;
            DbCommand comando = ComandoSQL(sql);
            try
            {
                comando.Connection.Open();
                resultado = comando.ExecuteScalar();
            }
            finally
            {
                if (comando.Connection != null) comando.Connection.Close();
            }
            return resultado;
        }

        public string GetJSON(string sqlProc, DbParameter[] parametros)
        {
            DataTable dt = getDataTable(sqlProc, parametros);
            return CovertToJSON(dt);
        }

        public string GetJSON(string sql)
        {
            DataTable dt = getDataTable(sql);
            return CovertToJSON(dt);
        }

        public string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros)
        {
            DataTable dt = getDataTable(sqlProc, parametros);
            return CovertToJSON_CampoValor(dt, null);
        }

        public string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros, string JsonName)
        {
            DataTable dt = getDataTable(sqlProc, parametros);
            return CovertToJSON_CampoValor(dt, JsonName.Trim());
        }

        public string GetJSON_CampoValor(string sql)
        {
            DataTable dt = getDataTable(sql);
            return CovertToJSON_CampoValor(dt, null);
        }

        public string GetJSON_CampoValor(string sql, string JsonName)
        {
            DataTable dt = getDataTable(sql);
            return CovertToJSON_CampoValor(dt, JsonName.Trim());
        }

        public static string CovertToJSON_CampoValor(DataTable dt, string JsonName)
        {
            string sTabla = "[";
            string sRegistro = "";
            bool swIni = false;

            foreach (DataRow row in dt.Rows)
            {
                sRegistro += "{"; swIni = true;
                foreach (DataColumn column in dt.Columns)
                {
                    if (column.ColumnName.Trim().Contains("Campo"))
                    {
                        sRegistro += (swIni) ? "" : ",";
                        sRegistro += "\"" + row[column].ToString().Trim().Replace("\"", "") + "\"" + ":";
                    }
                    if (column.ColumnName.Trim().Contains("Valor"))
                    {
                        sRegistro += "\"" + row[column].ToString().Trim().Replace("\"", "") + "\"";
                    }
                    swIni = false;
                }
                sRegistro += "}";

                sTabla += (sTabla != "[") ? "," : "";
                sTabla += sRegistro;
                sRegistro = "";
            }
            sTabla = sTabla != "[" ? "{\"" + JsonName ?? "Json" + "\":" + sTabla + "]}" : string.Empty;
            return UnicodeToUtf8(sTabla);
        }

        public static string CovertToJSON(object objToSerialize)
        {
            return UnicodeToUtf8(JsonConvert.SerializeObject(objToSerialize, Formatting.Indented));
        }

        public string CovertToJSON(DataTable dt, string JsonName)
        {
            string sTabla = "[";
            string sRegistro = "";

            foreach (DataRow row in dt.Rows)
            {
                sRegistro += "{";
                foreach (DataColumn column in dt.Columns)
                {
                    sRegistro += (sRegistro != "{") ? "," : "";
                    sRegistro += "\"" + column.ColumnName.Trim() + "\"" + ":" + "\"" + row[column].ToString().Trim().Replace("\"", "") + "\"";
                }
                sRegistro += "}";

                sTabla += (sTabla != "[") ? "," : "";
                sTabla += sRegistro;
                sRegistro = "";
            }
            sTabla = sTabla != "[" ? "{\"" + JsonName ?? "Json" + "\":" + sTabla + "]}" : string.Empty;
            return UnicodeToUtf8(sTabla);
        }

        public static string UnicodeToUtf8(string unicodeString)
        {
            Encoding utf8 = Encoding.UTF8;
            Encoding unicode = Encoding.Unicode;

            byte[] unicodeBytes = unicode.GetBytes(unicodeString);
            byte[] utf8Bytes = Encoding.Convert(unicode, utf8, unicodeBytes);

            char[] utf8Chars = new char[utf8.GetCharCount(utf8Bytes, 0, utf8Bytes.Length)];
            utf8.GetChars(utf8Bytes, 0, utf8Bytes.Length, utf8Chars, 0);

            return new string(utf8Chars);
        }
        /// <summary>
        /// Indica si el valor de una columna contenida en un SqlDataReader es igual a null
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="nombreColumna"></param>
        /// <returns></returns>
        public bool EsValorNulo(SqlDataReader reader, string nombreColumna)
        {
            if (reader[nombreColumna] == System.DBNull.Value)
                return true;
            else
                return false;
        }    
    }
}
