﻿
using Loop.Data.Interface;
using System;
using System.Configuration;

namespace Loop.Data.DatabaseConnection
{
    public class DataBaseConnection 
    {
        public IConnection GetConnectionDB(string databaseEngine)
        {
            if (databaseEngine.ToLower().Equals(Enum.GetName(typeof(EnumDatabase), EnumDatabase.SQLSERVER).ToLower()))
                return new SQLServerConnection();
            else if (databaseEngine.ToLower().Equals(Enum.GetName(typeof(EnumDatabase), EnumDatabase.MYSQL).ToLower()))
                return new MySQLConnection();
            else return null;
        }
    }
}
