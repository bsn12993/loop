﻿using System.Data;

namespace Loop.Data.DatabaseConnection
{
    public class DatabaseConnectionParams
    {
        public string CadConexion { get; set; }
        public int TiempoEspera { get; set; }
        public CommandType TipoConsulta { get; set; }
    }
}
