﻿using Loop.Core.Data;
using Loop.Core.Domain;
using Loop.Core.Models;
using Loop.Data.DatabaseConnection;
using Loop.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.DL
{
    public class DL_Catalog : DL_Base, ICatalogBase<Catalog>
    {
        public DL_Catalog()
        {
            var dataBaseConnection = new DataBaseConnection();
            Connection = dataBaseConnection.GetConnectionDB(DataSettings.DatabaseEngine);
        }

        public Response Delete(int id, string table)
        {
            parameters = new DbParameter[]
            {
                new SqlParameter("@id",id),
                new SqlParameter("@deleted_at",DateTime.Now),
                new SqlParameter("@table",table)
            };
            Query = @"update @table set deleted_at = @deleted_at where id = @id";
            Result = Connection.CargarQuery(Query,parameters);
            throw new NotImplementedException();
        }

        public Response FindAll(string table)
        {
            parameters = new DbParameter[]
             {
                new SqlParameter("@table",table)
             };
            Query = @"select * @table where deleted_at <> null";
            Result = Connection.CargarQuery(Query, parameters);
            throw new NotImplementedException();
        }

        public Response FindById(int id, string table)
        {
            parameters = new DbParameter[]
            {
                new SqlParameter("@table",table),
                new SqlParameter("@id",id)
            };
            Query = @"select * @table where id = @id and deleted_at <> null";
            Result = Connection.CargarQuery(Query, parameters);
            throw new NotImplementedException();
        }

        public Response Insert(Catalog obj, string table)
        {
            parameters = new DbParameter[]
            {
                new SqlParameter("@table",table),
                new SqlParameter("@name",obj.Name),
                new SqlParameter("@status",obj.Status),
                new SqlParameter("@created_at",obj.CreatedAt)
            };
            Query = @"insert into @table (name,status,created_at) values(@name,@status,@created_at)";
            Result = Connection.CargarQuery(Query, parameters);
            throw new NotImplementedException();
        }

        public Response Update(Catalog obj, int id, string table)
        {
            parameters = new DbParameter[]
            {
                new SqlParameter("@table",table),
                new SqlParameter("@name",obj.Name),
                new SqlParameter("@status",obj.Status),
                new SqlParameter("@updated_at",obj.UpdatedAt)
            };
            Query = @"update @table set name=@name,status=@status,updated_at=@updated_at where id=@id";
            Result = Connection.CargarQuery(Query, parameters); 
            throw new NotImplementedException();
        }
    }
}
