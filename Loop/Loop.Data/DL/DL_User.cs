﻿using Loop.Core.Data;
using Loop.Core.Domain;
using Loop.Core.Models;
using Loop.Data.DatabaseConnection;
using Loop.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.DL
{
    public class DL_User : DL_Base, IDL_User
    {

        public DL_User()
        {
            var dataBaseConnection = new DataBaseConnection();
            Connection = dataBaseConnection.GetConnectionDB(DataSettings.DatabaseEngine);  
        }

        public Response AuthenticateUser(string user, string password)
        {
            parameters = new SqlParameter[]
            {
                 new SqlParameter("@user",user),
                 new SqlParameter("@password",password)
            };

            var json = Connection.GetJSON("select * from usuarios");


            return new Response
            {
                IsSuccess = true,
                Message = "",
                Result = json
            };
        }

        public Response Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Response FindAll()
        {
            throw new NotImplementedException();
        }

        public Response FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Response Insert(User obj)
        {
            throw new NotImplementedException();
        }

        public Response Update(User obj, int id)
        {
            throw new NotImplementedException();
        }
    }
}
