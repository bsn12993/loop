﻿using Loop.Data.Interface;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.DL
{
    public abstract class DL_Base
    {
        public DbParameter[] parameters = null;
        public IConnection Connection = null;
        public string Query = string.Empty;
        public int Result = 0;
    }
}
