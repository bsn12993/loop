﻿using Loop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.Interface
{
    public interface ICatalogBase<T>
    {
        /// <summary>
        /// Realiza una búsqueda completa de todos los registros existentes
        /// </summary>
        /// <returns></returns>
        Response FindAll(string table);
        /// <summary>
        /// Realiza una búsqueda especificando el registro a buscar por su id
        /// </summary>
        /// <param name="id">id del registro</param>
        /// <returns>Objeto Response con los datos de la consulta</returns>
        Response FindById(int id, string table);
        /// <summary>
        /// Realiza un insert de un nuevo registro
        /// </summary>
        /// <param name="obj">Instancia de un clase</param>
        /// <returns>Objeto Response con la respuesta de la operación</returns>
        Response Insert(T obj, string table);
        /// <summary>
        /// Realiza una actualización de un registro existente
        /// </summary>
        /// <param name="obj">Instancia de un clase</param>
        /// <param name="id">id del registro</param>
        /// <returns>Objeto Response con la respuesta de la operación</returns>
        Response Update(T obj, int id, string table);
        /// <summary>
        /// Realiza una eliminación de un registro 
        /// Puede ser borrado logico o fisico
        /// </summary>
        /// <param name="id">id del registro</param>
        /// <returns>Objeto Response con la respuesta de la operación</returns>
        Response Delete(int id, string table);
    }
}
