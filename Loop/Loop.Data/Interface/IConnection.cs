﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.Interface
{
    public interface IConnection 
    {
        /// <summary>
        /// Establece la conexión por default
        /// </summary>
        void DbConnectionDefault();
        /// <summary>
        /// Establece la conexión en base de  datos con una connectionstring especifica
        /// </summary>
        /// <param name="keyCadConexion"></param>
        void DbConnectionConnectionString(string keyCadConexion);
        /// <summary>
        /// Establece la conexión en base de datos con parametros especificos
        /// </summary>
        /// <param name="host">servidor</param>
        /// <param name="bd">base de datos</param>
        /// <param name="sspi">seguridad</param>
        /// <param name="user">usuario</param>
        /// <param name="password">contraseña del usuario</param>
        void DbConnectionParams(string host, string bd, bool sspi, string user = null, string password = null);
        /// <summary>
        /// Realiza una conexión a la base de datos como prueba
        /// </summary>
        /// <returns>true si la conexión se realizo correctamente</returns>
        bool ProbarConexion();
        /// <summary>
        /// Carga un query especifico retornando el numero de filas afectadas 
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <returns>numero de columnas afectadas</returns>
        int CargarQuery(string sql);
        /// <summary>
        /// Realiza un insert a una tabla retornando el id del registro nuevo
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <returns>Id del registro insertado</returns>
        int insertReturnID(string sql);
        /// <summary>
        /// Ejecuta la consulta y retorna el primera fila de la respuesta
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <returns>resultado de la consulta</returns>
        object getScalar(string sql);
        //string GetJSON_CampoValor(string sql);
        //string GetJSON_CampoValor(string sql, string JsonName);
        //string CovertToJSON_CampoValor(DataTable  dt, string JsonName);
        //string CovertToJSON(object objToSerialize);
        //string CovertToJSON(DataTable dt, string JsonName);
        //string UnicodeToUtf8(string unicodeString);
        string GetJSON(string sql);
        /// <summary>
        /// Obtiene la conexión a la base de datos
        /// </summary>
        /// <returns>Objecto de la conexión</returns>
        DbConnection ObtenerConexion();
        /// <summary>
        /// Ejecuta un procedimiento almacenado con parametro
        /// </summary>
        /// <param name="sqlProc">nombre del procedimiento almacenado</param>
        /// <param name="Parametros">arreglo de parametros del procedimiento</param>
        /// <returns>Objecto del Command</returns>
        DbCommand ComandoProc(string sqlProc, DbParameter[] Parametros);
        /// <summary>
        /// Ejecuta consulta sin parametros
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <returns>Objecto del Command</returns>
        DbCommand ComandoSQL(string sql);
        DbCommand ComandoTransact();
        /// <summary>
        /// Realiza la consulta del query retornando una datareadar con arreglo de parametros
        /// </summary>
        /// <param name="sqlProc">nombre del procedimiento o consulta sql</param>
        /// <param name="parametros">arreglo de parametros de la consulta</param>
        /// <returns>Objecto del DataReader</returns>
        DbDataReader getReader(string sqlProc, DbParameter[] parametros);
        /// <summary>
        /// Realiza la consulta del query retornando una datareadar sin arreglo de parametros
        /// </summary>
        /// <param name="sql">consulta sql o nombre del procedimiento</param>
        /// <returns>Objecto del DataReader</returns>
        DbDataReader getReader(string sql);
        /// <summary>
        /// Obtiene los datos de la consulta con parametros en un Datable
        /// </summary>
        /// <param name="sqlProc">nombre de procedimiento o consulta sql</param>
        /// <param name="parametros">arreglo de parametros</param>
        /// <returns>Objeto DataTable con los registros</returns>
        DataTable getDataTable(string sqlProc, DbParameter[] parametros);
        /// <summary>
        /// Obtiene los datos de la consulta en un Datable
        /// </summary>
        /// <param name="sql">consulta sql o nombre del procedimiento almacenado</param>
        /// <returns>Objeto DataTable con los registros de la consulta</returns>
        DataTable getDataTable(string sql);
        /// <summary>
        /// Carga un query con parametros especificos retornando el numero de filas afectadas 
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <param name="parametros">arreglo de parametros</param>
        /// <returns>numero de filas afectadas</returns>
        int CargarQuery(string sqlProc, DbParameter[] parametros);
        /// <summary>
        /// Realiza un insert a una tabla retornando el id del registro nuevo
        /// </summary>
        /// <param name="sql">consulta sql o nombre de procedimiento almacenado</param>
        /// <returns>ID del registro insertado</returns>
        int insertReturnID(string sql, DbParameter[] parametros);
        /// <summary>
        /// Ejecuta multiples querys en un solo metodo
        /// </summary>
        /// <param name="querys">consulta sql</param>
        /// <returns>true si las consultas se ejecutaron correctamente</returns>
        bool insertQuerys(List<string> querys);
        /// <summary>
        /// Ejecuta la consulta con parametros y retorna el primera fila de la respuesta
        /// </summary>
        /// <param name="sql">consulta sql</param>
        /// <returns>Objeto con el registro afectado</returns>
        object getScalar(string sqlProc, DbParameter[] parametros);
        //string GetJSON(string sqlProc, DbParameter[] parametros);
        //string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros);
        //string GetJSON_CampoValor(string sqlProc, DbParameter[] parametros, string JsonName);
        //bool EsValorNulo(DbDataReader reader, string nombreColumna);
    }
}
