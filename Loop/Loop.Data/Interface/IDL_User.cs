﻿using Loop.Core.Domain;
using Loop.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop.Data.Interface
{
    public interface IDL_User : IDataAccess<User>
    {
        Response AuthenticateUser(string user, string password);
    }
}
